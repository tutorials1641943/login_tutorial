import express from "express";
import session from "express-session";
import RedisStore from "connect-redis";
import { createClient } from "redis";
import { cmpPassword } from "./src/login.js";
import loginRouter from "./src/loginRouter.js";

const app = express();
const port = 3333;

// create redis client
const client = createClient(6380);
client.connect();
const redisStore = new RedisStore({
	client: client,
	prefix: "login-tutorial:"
})

app.use(session({
	secret: "secret",
	saveUninitialized: true,
	resave: false,
	store: redisStore
}))

app.use(express.json());

app.get('/api/', (req, res) => {
	res.send("Hello World");
})

app.use('/api/login', loginRouter);

app.listen(port, () => {
	console.log(`Server is running at port ${port}`);
})
