// to use this service, user should be logged in

const authenticatedService = (req, res) => {
    res.json({
        secret: "you are mango cookie!",
        message: "you are seeing this message if you are logged in"
    })
}

export default authenticatedService;