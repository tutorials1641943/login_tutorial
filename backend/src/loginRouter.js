import { Router } from "express";
import { cmpPassword, cmpUsername } from "./login.js";

const loginRouter = Router();

loginRouter.get('/', (req, res) => {
    console.log("GET /api/login")
    if (req.session.username) {
		res.json({
			username: req.session.username,
			message: "Already logged in",
		})
	} else {
		res.json({
			username: null,
			message: "You need to log in first",
		})
	}
})

loginRouter.post('/', (req, res) => {
    console.log("POST /api/login")
    const username = req.body.username;
    const password = req.body.password;

    if(cmpUsername(username) && cmpPassword(password)) {
        // login success
        // set session
        req.session.username = username;

        res.json({
            username: username,
            message: "Login success",
        })
    } else {
        //login failed 
        res.json({
            username: null,
            message: "Login failed",
        })
    }
})

loginRouter.get('/logout', (req, res) => {
    console.log("GET /api/login/logout")
    req.session.destroy((err) => {
        if(err) {
            res.json({
                message: "Logout failed",
                success: false,
            })
        } else {
            res.clearCookie("connect.sid");
            res.json({
                message: "Logout success",
                success: true,
            })
        }
    })
})

export default loginRouter;