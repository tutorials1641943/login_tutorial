// login middleware that checks if user is logged in 

const loginMiddleware = (req, res, next) => {
    if (req.session.username) {
        next();
    } else {
        res.json({
            username: null,
            message: "You need to log in first",
        })
    }
}

export default loginMiddleware;