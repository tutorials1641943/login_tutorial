// for simplicity, we will only check if username is john, and password is 12345 
// we will also use hash to compare password.
import sha256 from "crypto-js/sha256.js";

const USERNAME = "john";
const PASSWORD = "12345";

export const cmpUsername = (username) => {
    return username === USERNAME;
}

export const cmpPassword = (password) => {
    const hash1 = sha256(password);
    const hash2 = sha256(PASSWORD);
    return hash1.toString() === hash2.toString();
}