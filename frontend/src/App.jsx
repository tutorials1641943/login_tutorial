import { useState } from 'react'
import './App.css'
import { useEffect } from 'react';
import axios from 'axios';

function App() {
  const [username, setUsername] = useState('');
  const [loginState, setLoginState] = useState(false);

  const [usernameInput, setUsernameInput] = useState('');
  const [passwordInput, setPasswordInput] = useState('');

  useEffect(() => {
    // initially check if user is already logged in
    axios.get('/api/login')
      .then((res) => {
        console.log(res.data)
        if (res.data.username) {
          setLoginState(true);
          setUsername(res.data.username);
        }
      })
  }, [])

  const usernameHandler = (e) => {
    setUsernameInput(e.target.value);
  }

  const passwordHandler = (e) => {
    setPasswordInput(e.target.value);
  }
  const loginHandler = () => {
    axios.post('/api/login', {
      username: usernameInput,
      password: passwordInput,
    })
      .then((res) => {
        console.log(res.data)
        if(res.data.username) {
          setLoginState(true);
          setUsername(res.data.username);
        } else {
          alert('wrong username or password');
        }
      })
      .catch((err) => {
        console.log(err);
      })
  }

  const logoutHandler = () => {
    axios.get('/api/login/logout')
      .then((res) => {
        if(res.data.success) {
          setLoginState(false);
          setUsername('');
          alert('logout success')
        } else {
          alert('logout failed');
        }
      })
  }

  return (
    <div>
      <h3>is user logged in? {`${loginState}`}</h3>
      <br/>
      <div>
        <h2>Login!!</h2>

        <input type="text" placeholder='username' onChange={usernameHandler}/>
        <br/>
        <input type="text" placeholder='password' onChange={passwordHandler}/>
        <br/>
        <button onClick={loginHandler}>Login</button>
      </div>

      <br/>
      <br/>
      <div>
        <button onClick={logoutHandler}>Logout!!</button>
      </div>
      
    </div>
  )
}

export default App
